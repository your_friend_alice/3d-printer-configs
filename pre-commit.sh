#!/usr/bin/env bash
set -e
here="$(dirname "$(readlink -f "$0")")"
root="$(git rev-parse --show-toplevel)"
[[ "$here" == "$root" ]] && cp "$0" "$root/.git/hooks/pre-commit"
bundle="$root/bundle.ini"
truncate --size 0 "$bundle"
first=true
for type in print filament printer physical_printer; do
    for item in "$root/$type"/*; do
        [[ $first = true ]] || echo >> "$bundle"
        echo "bundling file $item"
        echo "[$type:$(basename "$item" .ini)]" >> "$bundle"
        git cat-file blob ":$(realpath --relative-to "$root" "$item")" | grep -v "^#" >> "$bundle"
        first=false
    done
done
git add "$bundle"
