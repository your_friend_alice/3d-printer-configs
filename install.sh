#!/usr/bin/env bash
set -e
dest="$HOME/.config/PrusaSlicer"
here="$(dirname "$(readlink -f "$0")")"
for type in filament print printer physical_printer; do
    dir="$dest/$type"
    mkdir -p "$dir"
    for item in "$here/$type"/*; do
        ln -sf "$item" "$dir/$(basename "$item")"
    done
done
